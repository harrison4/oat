export const environment = {
  production: true,
  api: {
    base: 'https://hr.oat.taocloud.org/',
    version: 'v1/'
  },
};
