export const environment = {
  production: false,
  api: {
    base: 'https://hr.oat.taocloud.org/',
    version: 'v1/'
  }  
};