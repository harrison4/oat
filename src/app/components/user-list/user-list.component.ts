import { ViewBase } from 'app/interfaces/view.base';
import { Component} from '@angular/core';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html'
})

/**
 * User list component.
 * @description Main view for OATs user list feature
 * 
 * @author Jesús García
 * @version 1.0.0.0
 * @extends ViewBase
 */
export class UserListComponent extends ViewBase {

  // Default service
  service: string = 'http';
  // Users list
  users: any[];
  // User keys to show in list
  keys: string[] = ['firstname', 'lastname'];
  // Fetch sources
  fetchSources: string[] = ['api', 'csv', 'json'];
  // Active source
  activeSource:string = 'api';

  /**
   * Constructor
   */
  constructor() {
    super();   
  }

  /**
   * Change
   * @param source 
   */
  sourceChange(source:string)
  {        
    // If we are changing the source, we need to set the appropiate service for it

    switch(source)
    {
      case 'csv':
      case 'json':
        this.service = 'file';
        break;
      default: 
        this.service = 'http';
        break;
    }

    this.activeSource = source;    

    // Fetch data again from new source
    this.getData();
  }

  /**
   * Fetch data 
   * @override
   */
  getData():void
  {     
    this.dataSvc.setService(this.service).get('users', this.activeSource).subscribe(res => {
      this.users = res;
    });
  }
}