import { Component } from '@angular/core';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html'  
})

/**
 * Nav bar component.
 * @description Prints navigation bar
 * 
 * @author Jesús García
 * @version 1.0.0.0
 */
export class NavBarComponent{

}
