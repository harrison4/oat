import { Routes, RouterModule } from '@angular/router'
import { ModuleWithProviders } from '@angular/core'
import { HomeComponent } from 'app/views/home/home.component'
import { NotFoundComponent } from 'app/views/not-found/not-found.component'

/**
 * Here we set module routes for navigate through the app
 */
export const routes: Routes = [    
  { path: '', component: HomeComponent},    
  { path: '**', component: NotFoundComponent }
]

export const Routing = RouterModule.forRoot(routes)