import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})

/**
 * Home component.
 * @description Main view for OATs test project
 * 
 * @author Jesús García
 * @version 1.0.0.0
 * @extends ViewComponent
 */
export class HomeComponent {
}
