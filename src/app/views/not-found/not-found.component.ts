import { Component, OnInit } from '@angular/core';

@Component({
  templateUrl: './not-found.component.html'
})

/**
 * Not found component.
 * @description Not found screen for unrecognized routes
 * 
 * @author Jesús García
 * @version 1.0.0.0
 * @extends ViewComponent
 */
export class NotFoundComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
