import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { NgModule, Injector } from '@angular/core';
import { setAppInjector } from './app-injector';
import { Routing } from './app.routing';
import { ViewBase } from 'app/interfaces/view.base';
import { ElementBase } from 'app/interfaces/element.base';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { HttpService } from './services/http.service';
import { FileService } from './services/file.service';
import { DataService } from './services/data.service';
import { HomeComponent } from './views/home/home.component';
import { NotFoundComponent } from './views/not-found/not-found.component';
import { RadioListComponent } from './resources/elements/radio-list/radio-list.component';
import { UserListComponent } from './components/user-list/user-list.component';
import { ListComponent } from './resources/elements/list/list.component';

@NgModule({
  declarations: [
    ViewBase,
    ElementBase,
    AppComponent,
    NavBarComponent,
    HomeComponent,    
    NotFoundComponent,        
    ListComponent,
    RadioListComponent,
    UserListComponent   
  ],
  imports: [
    BrowserModule,    
    HttpClientModule,
    BrowserAnimationsModule,           
    FormsModule,
    Routing    
  ],  
  providers: [HttpService, FileService, DataService],
  bootstrap: [AppComponent]
})

export class AppModule { 

  constructor(private injector: Injector) 
  {    
    setAppInjector(injector);
  }
}
