import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { environment } from 'environments/environment';
import * as uuid from 'uuid';

@Component({
  template: 'No UI needed'
})

/**
 * Element base class.
 * 
 * @description Every resource elements inherit from this parent class.
 * 
 * @author Jesús García
 * @version 1.0.0.0 
 */
export class ElementBase implements OnChanges, OnInit {

  // Element DOM id
  public id;
  // App config   
  public config;
  // Element ready to show or not
  public isReady:Boolean = false;    

  /**
   * Constructor.   
   */
  constructor() {     
    this.config = environment;        
    this.id = uuid.v4();
  }  

  /**
   * Check ready 
   */
  checkReady()
  {    
      this.setReady(true);
      return this;
  }

  /**
   * Set ready state   
   * @param {Boolean} isReady 
   */
  setReady(isReady:Boolean)
  {
      this.isReady = isReady;
      return this;
  }

  
  /**
   * OnChanges event subscription
   * @param changes 
   */
  ngOnChanges(changes: SimpleChanges) {
    this.checkReady();
  }

  /**
   * Angular onInit lifecycle method   
   */
  ngOnInit()
  {
    this.checkReady();
  }
}