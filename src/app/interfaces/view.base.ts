import { Component, OnInit } from '@angular/core';
import { environment } from 'environments/environment';
import { DataService } from 'app/services/data.service';
import {AppInjector} from 'app/app-injector';

@Component({
  template: 'No UI needed'
})

/**
 * View base class.
 * 
 * @description All views inherit from this parent class.
 * 
 * @author Jesús García
 * @version 1.0.0.0 
 */
export class ViewBase implements OnInit {

  // App config   
  public config;

  // Data service
  public dataSvc:DataService;

  // Default data retrieval
  public method = 'http';

  /**
   * Constructor.   
   */
  constructor() {     
    this.config = environment;    
    this.dataSvc = AppInjector.get<DataService>(DataService);
  }  

  /**
   * Main get data method
   * 
   * @returns {Void}  
   */
  getData():void {   
  }
  

  /**
   * OnInit lifecycle method
   */
  ngOnInit() {
    this.getData();
  }

}
