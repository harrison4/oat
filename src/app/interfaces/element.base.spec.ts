import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElementBase } from './element.base';

describe('ElementBase', () => {
  let component: ElementBase;
  let fixture: ComponentFixture<ElementBase>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElementBase ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElementBase);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
