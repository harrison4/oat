import { Injectable  } from '@angular/core';

@Injectable()

/**
 * Data interface class. 
 * 
 * @author Jesús García
 * @version 1.0.0.0 
 */
export abstract class DataInterface {

  /**
   * Get method
   * @param source 
   */
  get(source:any){}    
}
