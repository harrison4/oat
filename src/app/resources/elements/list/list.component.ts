import { Component, Input, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import {ElementBase} from 'app/interfaces/element.base';

@Component({
  selector: 'list',
  templateUrl: './list.component.html'  
})


/**
 * List resource
 * 
 * @description Draws an item list
 * 
 * @author Jesús García
 * @version 1.0.0.0 
 */
export class ListComponent extends ElementBase implements OnInit, OnChanges {
  
  @Input() items = [];
  @Input() keys = [];

  // Show header or not
  showHeader:Boolean = true;
  
  /**
   * Constructor
   */
  constructor() {
    super();
  }  

  /**
   * Check ready 
   */
  checkReady() {    
      // If we have the keys setted, we can set element as ready.
      
      if (this.keys)
      {
        this.setReady(true);
      }

      return this;
  }

  /**
   * OnChanges event subscription
   * @param changes 
   */
  ngOnChanges(changes: SimpleChanges) {
    
    // Check if we have already loaded any item into currentValues.
    // If true, set list keys
    if (typeof(changes.items.currentValue) != "undefined" && 
        changes.items.currentValue instanceof Array &&
        changes.items.currentValue.length > 0)
    {
      if (!this.keys.length)
      {
        this.keys = Object.keys(changes.items.currentValue[0]);
      }      
    }

    this.checkReady();
  }
}
