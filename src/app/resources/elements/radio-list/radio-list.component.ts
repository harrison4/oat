import { Component, Input, Output, EventEmitter, OnInit, OnChanges, ɵConsole } from '@angular/core';
import {ElementBase} from 'app/interfaces/element.base';

@Component({
  selector: 'radio-list',
  templateUrl: './radio-list.component.html'
})

/**
 * Radio list
 * 
 * @description Draws an input radio list
 * 
 * @author Jesús García
 * @version 1.0.0.0 
 */
export class RadioListComponent extends ElementBase implements OnInit, OnChanges {
  
  @Input() name:string = null;
  @Input() items = [];
  @Input() defaultChoice:string = null;

  @Output() model: EventEmitter<any> = new EventEmitter();
  
  /**
   * Constructor
   */
  constructor() {
    super();
  }

  /**
   * Angular onInit lifecycle method
   */
  ngOnInit()
  {
    if (!this.name)
    {
      this.name = this.id;
    }

    this.checkReady();
  }

  /**
   * Select method 
   * @param {String} value
   */
  select(value:string)
  {
    this.model.emit(value);
  }
}
