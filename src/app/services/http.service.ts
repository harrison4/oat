import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DataInterface } from 'app/interfaces/data.interface';
import { environment } from 'environments/environment';
import { AppInjector } from 'app/app-injector';

@Injectable()

/**
 * Http Service.
 * @description Service in charge of HTTP requests
 * 
 * @author Jesús García
 * @version 1.0.0.0
 * @implements {DataInterface}
 */
export class HttpService implements DataInterface {

  private apiUrl: String;
  private http : HttpClient;

  /**
   * Api entities
   */
  entities:Object = {
    users: {
      endpoint: 'users',
    }
  }

  /**
   * Constructor   
   */
  constructor() {   
    
    this.apiUrl = environment.api.base + environment.api.version;
    this.http = AppInjector.get<HttpClient>(HttpClient);
  }

  /**
   * Retrieve endpoint
   * @param method 
   */
  getEndpoint(method:string)
  {
    // Here we can handler filters, params or whatever we need previous to server request    
    return this.apiUrl + this.entities[method].endpoint;
  }

  /**
   * Retrieve data from an url
   * @param {string} url Url
   */
  getUrl(url:string):Observable<Object>
  {
    return this.http.get(url);
  }  

  /**
   * Get method
   * 
   * @param {string} method Api method
   */
  get(method:string = null):Observable<any>{
    
    let endpoint = this.getEndpoint(method);
    
    return this.http.get(endpoint).pipe( map(res => this.parseResponse(method, res)) );
  }

  /**
   * Parse response. 
   * 
   * @param {string} method Request method
   * @param {any} response 
   */
  parseResponse(method:string, response:any)
  {    
    let parsed:any = [];

    switch(method)
    {
      // Lowercase fields to make them compatible with file headers.
      case 'users':
        response.map(user => {
          let newUser = {};
          Object.keys(user).map(key => {
            newUser[key.toLocaleLowerCase()] = user[key];
          })

          parsed.push(newUser);
        });
        break;
      default:
        // Return the same response
        parsed = response;
        break;
    }
    // If we need to do some parse from data, do it before returning it
    return parsed;
  }
}
