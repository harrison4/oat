import { Injectable  } from '@angular/core';
import { HttpService } from './http.service';
import { FileService } from './file.service';
import {AppInjector} from 'app/app-injector';
import { Observable } from 'rxjs';

@Injectable()

/**
 * Data loader base class. 
 * 
 * @description Data service who controls all existing services to let choose between them
 * 
 * @author Jesús García
 * @version 1.0.0.0 
 */
export class DataService {

  // Default data retrieva method
  service:string = 'http';

  /**
   * @property {HttpService} http
   * @property {FileService} file
   */
  public services = {http: null, 
                     file: null};
  
  /**
   * Constructor   
   */
  constructor()
  {
    this.services.http = AppInjector.get<HttpService>(HttpService);
    this.services.file = AppInjector.get<FileService>(FileService);    
  }

  /**
   * Sets data service
   * @param service 
   * 
   * @returns DataService
   */
  setService(service:string):DataService
  {
    this.service = service;
    return this;
  }

  /**
   * Get url method.
   * 
   * @param {string} method Request method. Example: getUsers
   * @param {array} args Array of arguments
   */
  getUrl(method:string = null, ...args:any[]):Observable<any[]> {
    
    return this.services[this.service].getUrl(method, args);
  }

  
  /**
   * Get method.
   * @description Use for API data
   * 
   * @param {string} method Request method. Example: getUsers
   * @param {array} args Array of arguments
   */
  get(method:string = null, ...args:any[]):Observable<any[]> {
    
    return this.services[this.service].get(method, ...args);
  }  
}
