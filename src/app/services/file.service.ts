import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DataInterface } from 'app/interfaces/data.interface';
import { AppInjector } from 'app/app-injector';
import { HttpHeaders } from '@angular/common/http';
import { Papa } from 'ngx-papaparse';

@Injectable()

/**
 * File Service.
 * @description Service in charge of file data retrievals. 
 *              Although this is for demo purposes only, file load
 *              could be done with HttpService:getFile method instead of this,
 *              using HttpClient
 * 
 * @author Jesús García
 * @version 1.0.0.0
 * @implements {DataInterface}
 */
export class FileService implements DataInterface {

  private http : HttpClient;
  private csv : Papa;
  private httpOptions = null;

  /**
   * Data entities
   */
  entities:Object = {
    users: {
      json: 'assets/data/json/testtakers.json',
      csv: 'assets/data/csv/testtakers.csv',
    }
  }

  /**
   * Constructor   
   */
  constructor() { 
    this.http = AppInjector.get<HttpClient>(HttpClient);    
    this.csv = AppInjector.get<Papa>(Papa);   
  }

  /**
   * Set HTTP Options for request
   * 
   * @param {string} source
   */
  setHttpOptions(source:string)
  {
    if (!this.httpOptions)
    {
      this.httpOptions = {
          headers: new HttpHeaders({
            'Accept': 'text/' + source            
          }), 
          responseType: 'text'
      };

    } else {
      this.httpOptions.headers = this.httpOptions.headers.set('Accept', 'text/' + source);
    }
  }


  /**
   * Retrieve endpoint
   * @param method  Request method
   * @param source Source. Can be csv or json
   */
  getEndpoint(method:string, source:string)
  {
    // Here we can handler filters, params or whatever we need previous to server request    
    return this.entities[method][source];
  }
  
  /**
   * Get method
   * 
   * @param {string} method Api method
   * @param {string} source Source. CSV or JSON file
   */
  get(method:string = null, source:string = null):Observable<any>{ 
    
    let endpoint = this.getEndpoint(method, source);   
    
    this.setHttpOptions(source);
        
    return new Observable(observer => {    
      this.http.get(endpoint, this.httpOptions).subscribe(res => {
        this.parseResponse(method, res, source).subscribe(parsed => {
          observer.next(parsed);          
          observer.complete();
        });        
      });
    });
  }  

  /**
   * Parse response. 
   * 
   * @param {string} method Request method
   * @param {any} response Data retrieved.
   * @param {source} string Source. CSV or JSON
   * 
   * @return {Observable}
   */
  parseResponse(method:string, response:any, source:string):Observable<any[]>
  {    
    return new Observable(observer => {    
      let options:any = {};

      switch (source)
      {
        case 'csv':        
          options.header = true;
          options.complete = (results, file) => {                                    

            observer.next(results.data);
            observer.complete();

          };
          this.csv.parse(response, options);
          break;
        case 'json':
          observer.next(JSON.parse(response));
          observer.complete();
          break;
      }    
    });    
  }
}  
